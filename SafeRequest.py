import requests

def safe_request(url, req_type = "GET" , data = None, time_out = 10, **kwargs):
    try:
        resp = requests.request(req_type.upper(), url, data = data, timeout = time_out)

        resp.raise_for_status()

        try:
            return resp.json()
        except:
            return resp.text
    except requests.exceptions.Timeout:
        print("Server tardó demasiado en responder")
        return
    except requests.exceptions.HTTPError:
        code = resp.status_code
        print(f"Bad Request ({code=})")
        return

