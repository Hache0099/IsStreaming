import requests
import NotifyWidget
from SafeRequest import safe_request


def main():
    BASE_URL = "https://pipedapi.kavin.rocks/channel/UCoSrY_IQQVpmIRZ9Xf-y93g"
    CHECK_VID_URL = "https://pipedapi.kavin.rocks/streams/"
    
    channel_json = safe_request(BASE_URL)

    if channel_json is str:
        print("Respuesta es un puto text")
        return
    elif channel_json is None:
        print(type(channel_json))
        print("dunno pero no es un JSON")

        return

    latest = channel_json["relatedStreams"][0]

    vid_id = latest["url"].replace("/watch?v=","")

    vid_check = safe_request(CHECK_VID_URL + vid_id)

    if vid_check == None:
        return

    if vid_check["livestream"]:
        app = NotifyWidget.App(vid_check, vid_id)
        app.start()

    else:
        print("No")

if __name__ == "__main__":
    main()
